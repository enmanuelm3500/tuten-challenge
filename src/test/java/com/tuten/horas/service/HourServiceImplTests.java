package com.tuten.horas.service;

import com.tuten.horas.dto.ResponseDTO;
import com.tuten.horas.service.impl.HourServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;

import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class HourServiceImplTests {

    private static final LocalDateTime DEFAULTLOCALDATETIME = LocalDateTime.now();
    private static final String DEFAULTZONE = "-3";

    private static final String DEFAULTDATATIMESTRING = "15:03:01";


    @InjectMocks
    HourServiceImpl service;

    @Test
    public void testDateToUTCTimeStamp() {
        ResponseDTO result = service.dateToUTCTimeStamp(DEFAULTLOCALDATETIME, DEFAULTZONE);
        assertNotNull(result);
    }

    @Test
    public void testDateToUTCString() {
        ResponseDTO result = service.dateToUTCString(DEFAULTDATATIMESTRING, DEFAULTZONE);
        assertNotNull(result);
    }
}
