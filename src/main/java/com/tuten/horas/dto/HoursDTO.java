package com.tuten.horas.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class HoursDTO {

    @JsonProperty(value = "time",required = true)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
    @NotEmpty
    @NotNull
    private String time;

    @JsonProperty(value = "timezone", required = true)
    @NotEmpty
    @NotNull
    private String timezone;

    public HoursDTO() {
    }

    public HoursDTO(@NotEmpty @NotNull String time, @NotEmpty @NotNull String timezone) {
        this.time = time;
        this.timezone = timezone;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }
}
