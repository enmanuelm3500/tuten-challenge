package com.tuten.horas.controller;


import com.tuten.horas.dto.HoursDTO;
import com.tuten.horas.dto.HoursTimeStampDTO;
import com.tuten.horas.dto.ResponseDTO;
import com.tuten.horas.service.HourService;
import com.tuten.horas.service.impl.HourServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.http.HttpStatus.OK;

@RunWith(MockitoJUnitRunner.class)
public class HoursControllerTests {

    @Mock
    HoursTimeStampDTO hoursTimeStampDTO;

    @Mock
    HoursDTO hoursDTO;

    @Mock
    HourService service;

    @InjectMocks
    HoursController controller;

    @Test
    public void testCalculateHours() {
        ResponseEntity<ResponseDTO> result = controller.calculateHours(hoursDTO);
        assertNotNull(result);
        assertEquals(OK,result.getStatusCode());
    }

    @Test
    public void testDateToUTCString() {
        ResponseEntity<ResponseDTO> result = controller.calculateHours(hoursTimeStampDTO);
        assertNotNull(result);
        assertEquals(OK,result.getStatusCode());
    }

}
