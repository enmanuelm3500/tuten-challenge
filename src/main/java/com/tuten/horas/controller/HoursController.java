package com.tuten.horas.controller;


import com.tuten.horas.dto.HoursDTO;
import com.tuten.horas.dto.HoursTimeStampDTO;
import com.tuten.horas.dto.ResponseDTO;
import com.tuten.horas.service.HourService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/hours")
@CrossOrigin(origins = "*")
public class HoursController extends AbstractController{

    @Autowired
    private HourService service;

    @PostMapping
    public ResponseEntity<ResponseDTO> calculateHours(@Valid @RequestBody HoursDTO dto) {
        return createOkStatusResponseEntity(service.dateToUTCString(dto.getTime(), dto.getTimezone()));
    }

    @PostMapping("/by-timeStamp")
    public ResponseEntity<ResponseDTO> calculateHours(@Valid @RequestBody HoursTimeStampDTO dto) {
        return createOkStatusResponseEntity(service.dateToUTCTimeStamp(dto.getTime(), dto.getTimezone()));
    }



}
