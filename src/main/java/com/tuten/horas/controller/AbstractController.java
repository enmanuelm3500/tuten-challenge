package com.tuten.horas.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public abstract class AbstractController {

    protected ResponseEntity createOkStatusResponseEntity(Object body) {
        return new ResponseEntity<>(body, HttpStatus.OK);
    }

}
