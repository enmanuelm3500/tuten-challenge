package com.tuten.horas.service;

import com.tuten.horas.dto.ResponseDTO;

import java.time.LocalDateTime;

public interface HourService {

    ResponseDTO dateToUTCTimeStamp(LocalDateTime localDateTime, String timeZone);

    ResponseDTO dateToUTCString(String localDateTime, String timeZone);
}
