package com.tuten.horas.service.impl;

import com.tuten.horas.dto.HoursDTO;
import com.tuten.horas.dto.ResponseDTO;
import com.tuten.horas.service.HourService;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class HourServiceImpl implements HourService {

    @Override
    public ResponseDTO dateToUTCTimeStamp(LocalDateTime localDateTime, String timeZone) {
        ZonedDateTime zonedFechaExpiracion = ZonedDateTime
                .now(ZoneId.of(timeZone))
                .with(localDateTime)
                .withZoneSameInstant(ZoneOffset.UTC);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");

        return new ResponseDTO(generateDTO(zonedFechaExpiracion.format(formatter)));
    }

    @Override
    public ResponseDTO dateToUTCString(String localDateTime, String timeZone) {
        String[] hour = getHourSplinter(localDateTime);

        ZonedDateTime zonedFechaExpiracion = ZonedDateTime
                .now(ZoneId.of(timeZone))
                .withHour(Integer.parseInt(hour[0]))
                .withMinute(Integer.parseInt(hour[1]))
                .withSecond(Integer.parseInt(hour[2]))
                .withZoneSameInstant(ZoneOffset.UTC);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");

        return new ResponseDTO(generateDTO(zonedFechaExpiracion.format(formatter)));
    }

    private String[] getHourSplinter(String hours) {
        return hours.split(":");
    }

    private HoursDTO generateDTO(String hour) {
        return new HoursDTO(hour, "UTC");
    }


}
