               tuten-apis

Lenguaje de Programación: Java8,
Servicio Cloud utilizado para el despliegue: Heroku,
Url-Base: https://tuten-apis.herokuapp.com/
repositorio: git clone https://enmanuelm3500@bitbucket.org/enmanuelm3500/tuten-challenge.git

     Se Desarrollaron 2 apis, con la finalidad de calcular la hora en formato UTC, ambas se detallan a continuación:

url-api-1 : https://tuten-apis.herokuapp.com/hours

Request: 

{
    "time":"17:03:01",
    "timezone":"-1"
}

response Http.OK code: 200:

{
    "response": {
        "time": "18:03:01",
        "timezone": "UTC"
    }
}


url-api-2 : https://tuten-apis.herokuapp.com/hours/by-timeStamp

Request:
{
    "time":"2021-02-28T14:03:01.960",
    "timezone":"-1"
}

response Http.OK code: 200:

{
    "response": {
        "time": "15:03:01",
        "timezone": "UTC"
    }
}

    Ambas se diferencian en el atributo "time" del request, 	

    Para realizar las pruebas correspondientes se sugiere utilizar postman, se sugiere ademas descargue el repositorio y 
ubique la carpeta postman-collections, en ella encontrara los archivos "tuten-apis.postman_collection.json"y "tuten-apis-produccion.postman_environment.json"
los cuales debe importar en postam y posteriormente seleccionar request a probar (entre ambas apis), asi como 
el enviroment utilizar (tuten-api-production).
	
