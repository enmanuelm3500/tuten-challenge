package com.tuten.horas.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Optional;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class HoursTimeStampDTO {

    @JsonProperty(value = "time",required = true)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @NotEmpty
    @NotNull
    private LocalDateTime time;

    @JsonProperty(value = "timezone", required = true)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @NotEmpty
    @NotNull
    private String timezone;

    public HoursTimeStampDTO() {
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }
}
