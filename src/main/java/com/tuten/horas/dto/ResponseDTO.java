package com.tuten.horas.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseDTO {

    @JsonProperty(value = "response")
    private Object response;

    public ResponseDTO() {
    }

    public ResponseDTO(Object response) {
        this.response = response;
    }

    public Object getResponse() {
        return response;
    }

    public void setResponse(Object response) {
        this.response = response;
    }
}
